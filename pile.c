/* ------------------------------------------------------------------- */
/*                                                                     */
/*  pile.c   Contient toutes les définitions de fonctions des piles    */
/*                                                                     */
/* --------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include "pile.h"

/*Fonction permettant la création de la pile*/
/*Renvoie l'adresse de la pile*/
t_pile * InitPile(int taille)
{
    /*Allocation de la pile*/
    t_pile * ptPile = (t_pile *) malloc(sizeof(t_pile));
    /*Vérification que la pile à bien été allouée*/
    if (ptPile)
    {
        /*Insertion des données*/
        ptPile->taille = taille;
        ptPile->emplacement = -1;
        /*Allocation de la zone mémoire de la pile*/
        ptPile->p0 = (eltp *) malloc(taille*sizeof(eltp));
        /*Vérification que la zone mémoire à bien été allouée*/
        if (ptPile->p0 == NULL)
        {
            /*Si non allouée, on libère la pile*/
            LibererPile(ptPile);
            ptPile = NULL;            
        }
    }
    return ptPile;
}

/*Procédure qui permet de libérer la pile*/
void LibererPile(t_pile * ptP)
{
    int i;
    /*Boucle pour allant de 0 à emplacement*/
    for (i=0; i<=ptP->emplacement; i++)
    {
        free(&(ptP->p0[i]));
    }
    /*Libération du tableau*/
    free(ptP->p0);
    /*Libération de la pile*/
    free(ptP);
    ptP = NULL;
}

/*Fonction qui permet de vérifier si la pile est vide*/
/*Renvoie 1 si vide et 0 sinon*/
int EstVide(t_pile * ptP)
{
    return((ptP->emplacement == -1));
}

/*Fonction qui retourne si la pile est pleine*/
/*Retourne 1 si pleine, 0 sinon*/
int EstPleine(t_pile * ptF)
{
    return((ptF->emplacement == ptF->taille-1));
}

/*Fonction qui permet d'empiler une variable dans la pile*/
/*Renvoie un code d'erreur 0 si OK, 1 sinon*/
int Empiler(t_pile * ptP, eltp* v)
{
    int code = 1; /*OK: 0, Pas bon: 1*/
    /*Si emplacement n'est pas à la fin de l'espcace libre de la pile*/
    if (ptP->emplacement < ptP->taille-1)
    {
        /*Mise à jour emplacement*/
        ptP->emplacement = ((ptP->emplacement) + 1);
        /*Insertion de la variable dans la pile*/
        *((ptP->p0)+(ptP->emplacement)) = *v;
        /*Mise à jour code erreur*/
        code = 0;
    }
    return(code);
}

/*Fonction qui permet de dépiler un élément de la pile*/
/*Renvoie un code d'erreur 0 si OK, 1 sinon*/
int Depiler(t_pile * ptP, eltp ** ptV)
{
    int code = 1;/*OK: 0, Pas bon: 1*/
    /*Si la pile n'est pas vide*/
    if (EstVide(ptP) == 0)
    {
        /*Récupération de la valeur empilée*/
        *ptV =ptP->p0 + ptP->emplacement;
        /*Mise à jour emplacement*/
        ptP->emplacement = ptP->emplacement - 1;
        /*Mise à jour code erreur*/
        code = 0;
    }
    return(code);
}
