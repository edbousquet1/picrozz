/* --------------------------------------------------------------------------------------- */
/*                                                                                         */
/*  pile.h   Contient toutes les déclarations de fonctions de la pile                      */
/*                                                                                         */
/* ----------------------------------------------------------------------------------------*/

#ifndef pile_h
#define pile_h
#include <stdio.h>
#include <stdlib.h>

typedef struct element{
    int     positionX;         /*position en X*/
    int     positionY;         /*position en Y*/
    int     pinceau;           /*0 pour couleur et 1 pour croix*/
}eltp;

typedef struct pile{
    int     taille;         /*Taille de la pile*/
    int     emplacement;    /*Emplacement du dernier element de la pile*/
    eltp *  p0;             /*Pointeur sur le début de la pile*/
}t_pile;

/* -------------------------------------------------------------------- */
/* InitPile         Permet d'initialiser une pile                       */
/*                                                                      */
/* En entrée: La taille de la pile                                      */
/*                                                                      */
/* En sortie: L'adresse de la pile                                      */
/* -------------------------------------------------------------------- */
t_pile * InitPile(int);

/* -------------------------------------------------------------------- */
/* LibererPile         Permet de libérer une pile                       */
/*                                                                      */
/* En entrée: L'adresse de la pile                                      */
/*                                                                      */
/* En sortie: L'adresse de la pile                                      */
/* -------------------------------------------------------------------- */
void LibererPile(t_pile *);

/* -------------------------------------------------------------------- */
/* EstVide         Permet de savoir si une pile est vide                */
/*                                                                      */
/* En entrée: L'adresse de la pile                                      */
/*                                                                      */
/* En sortie: Un entier 1 si vide 0 sinon                               */
/* -------------------------------------------------------------------- */
int EstVide(t_pile *);

/* -------------------------------------------------------------------- */
/* EstPleine             Permet de savoir si une pile est pleine        */
/*                                                                      */
/* En entrée: L'adresse de la pile                                      */
/*                                                                      */
/* En sortie:  Un entier 1 si pleine 0 sinon                            */
/* -------------------------------------------------------------------- */
int EstPleine(t_pile *);

/* -------------------------------------------------------------------- */
/* Empiler             Permet d'empiler une valeur dans la pile         */
/*                                                                      */
/* En entrée: L'adresse de la pile et l'élément à empiler               */
/*                                                                      */
/* En sortie:  Un entier pour savoir si ça c'est bien passé 1 si oui    */
/* 0 sinon                                                              */
/* -------------------------------------------------------------------- */
int Empiler(t_pile *, eltp*);

/* -------------------------------------------------------------------- */
/* Depiler             Permet de deépiler une valeur de la pile         */
/*                                                                      */
/* En entrée: L'adresse de la pile et l'adresse dans lequel l'élément   */
/* sera récupérer                                                       */
/*                                                                      */
/* En sortie:  Un entier pour savoir si ça c'est bien passé 1 si oui    */
/* 0 sinon                                                              */
/* -------------------------------------------------------------------- */
int Depiler(t_pile *, eltp **);

#endif