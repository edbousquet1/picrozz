#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "picross.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>



int main()
{
	/*Variables choix*/
	int taille=5; 
	int vie = 1; /*1: mode en nombre de vies limitées //// 0 : sinon (avec UNDO)*/
	char prog[20];


    /*Declaration rectangle titre*/
    SDL_Rect rec_titre;
    rec_titre.x=100;
    rec_titre.y=0;
    rec_titre.w=500;
    rec_titre.h=120;

    /*Declaration rectangle Jouer*/
    SDL_Rect rec_jouer;
    rec_jouer.x=250;
    rec_jouer.y=550;
    rec_jouer.w=200;
    rec_jouer.h=75;

	/*Declaration rectangle Credit*/
    SDL_Rect rec_credit;
    rec_credit.x=25;
    rec_credit.y=650;
    rec_credit.w=100;
    rec_credit.h=50;

	/*Déclaration rectangles Taille*/
	SDL_Rect rect_titre_taille;

	rect_titre_taille.x=125;
	rect_titre_taille.y=200;
	rect_titre_taille.w=100;
	rect_titre_taille.h=50;

	SDL_Rect rect_taille_5;
	rect_taille_5.x=125;
	rect_taille_5.y=275;
	rect_taille_5.w=100;
	rect_taille_5.h=50;

	SDL_Rect rect_taille_10;
	rect_taille_10.x=125;
	rect_taille_10.y=350;
	rect_taille_10.w=100;
	rect_taille_10.h=50;

	SDL_Rect rect_taille_15;
	rect_taille_15.x=125;
	rect_taille_15.y=425;
	rect_taille_15.w=100;
	rect_taille_15.h=50;

	SDL_Rect rect_choix_taille;
	rect_choix_taille.x=120;
	rect_choix_taille.y=425;
	rect_choix_taille.w=110;
	rect_choix_taille.h=50;

	/*Déclaration rectancle vies*/
	SDL_Rect rect_titre_vie;

	rect_titre_vie.x=450;
	rect_titre_vie.y=200;
	rect_titre_vie.w=100;
	rect_titre_vie.h=50;

	SDL_Rect rect_avec_vie;
	rect_avec_vie.x=450;
	rect_avec_vie.y=275;
	rect_avec_vie.w=100;
	rect_avec_vie.h=50;

	SDL_Rect rect_sans_vie;
	rect_sans_vie.x=450;
	rect_sans_vie.y=350;
	rect_sans_vie.w=100;
	rect_sans_vie.h=50;

	SDL_Rect rect_choix_vie;
	rect_choix_vie.x=445;
	rect_choix_vie.y=275;
	rect_choix_vie.w=110;
	rect_choix_vie.h=50;


	/*Déclaration rec info mode de jeu*/
	SDL_Rect rect_info_mode;
	rect_info_mode.x=560;
	rect_info_mode.y=215;
	rect_info_mode.w=30;
	rect_info_mode.h=30;

    /*Déclaration de la fenetre*/
    SDL_Window *window;

	/*Déclaration du pinceau*/
	SDL_Renderer *renderer;
    
	/*Déclaration de la position de la souris*/
	SDL_Point position_mouse;
    
	/*Déclaration des éléments de la fenetre*/
   	int running = 1, width = 700, height = 700;


	/*Déclaration des événements*/
	SDL_Event event;

    /*Déclaration Background*/
    SDL_Texture *avatar;
	SDL_Rect rect;
  
    /*Initialisation du SDL*/
	if (SDL_Init(SDL_INIT_EVERYTHING) == -1)
	{
		fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError());
		return EXIT_FAILURE;
	}

	/*Initialisation du TTF*/
	if (TTF_Init() != 0)
	{
		fprintf(stderr, "Erreur d'initialisation TTF : %s\n", TTF_GetError());
	}


	/*Initialisation de la fenetre*/
	window = SDL_CreateWindow("PicroZZ", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_RESIZABLE);
	if (window == 0)
	{
		fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError());
	}
	

	/*Initialisation du pinceau*/
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED ); /*  SDL_RENDERER_SOFTWARE */
    if (renderer == 0) {
		 fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError());
		 /* faire ce qu'il faut pour quitter proprement */
	}

	/*Fixation de la taille fenetre*/
	SDL_SetWindowResizable(window,0);


    /*Affichage Background*/
    SDL_Surface *image = NULL;
	image=IMG_Load("image/fondMenu.jpg");
	
    avatar = SDL_CreateTextureFromSurface(renderer, image);
	SDL_FreeSurface(image);
	rect.x = 0;
	rect.y = 0;
	rect.w = 700;
    rect.h = 700;
	SDL_RenderCopy(renderer, avatar, NULL, &rect);
	SDL_RenderPresent(renderer);
    /*Récupération de la police*/
	TTF_Font* font = TTF_OpenFont("font/bonzai.ttf", 300);

	/*Initialisation  Titre*/
	affich_rect("Bienvenu sur PicroZZ",rec_titre,renderer,font);

    /*Initialisation rectangle jouer*/
	affich_rect("JOUEZ",rec_jouer,renderer,font);

	/*Initialisation rectangle Taille*/
	affich_rect("TAILLE",rect_titre_taille,renderer,font);

	/*Initialisation rectangle Credit*/
	affich_rect("Credit",rec_credit,renderer,font);
	
	/*Initialisation rectangle Mode*/
	affich_rect("VIE",rect_titre_vie,renderer,font);

	/*Initialisation rectangle info mode jeu*/
	affich_rect(" ? ",rect_info_mode,renderer,font);

	/*Initialisation rectangle 5*5*/
	affich_rect("5 * 5",rect_taille_5,renderer,font);

	/*Initialisation rectangle 10*10*/
	affich_rect("10 * 10",rect_taille_10,renderer,font);

	/*Initialisation rectangle 15*15*/
	affich_rect("15 * 15",rect_taille_15,renderer,font);

	/*Initialisation rectangle choix mode de jeu*/
	affich_rect("avec",rect_avec_vie,renderer,font);
	affich_rect("sans",rect_sans_vie,renderer,font);



	/*Initialisation rectangle choix taille*/
	SDL_SetRenderDrawColor(renderer, 0,0,0,0);
	SDL_RenderDrawRect(renderer, &rect_choix_taille);
	rect_choix_taille.y=350;
	SDL_SetRenderDrawColor(renderer, 0,0,0,0);
	SDL_RenderDrawRect(renderer, &rect_choix_taille);

	rect_choix_taille.y=275;
	SDL_SetRenderDrawColor(renderer, 255,255,255,0);
	SDL_RenderDrawRect(renderer, &rect_choix_taille);

	/*Dessin rectangle info mode*/
	SDL_SetRenderDrawColor(renderer, 255,255,255,0);
	SDL_RenderDrawRect(renderer, &rect_info_mode);
	SDL_SetRenderDrawColor(renderer, 255,255,255,0);
	SDL_RenderDrawRect(renderer, &rect_choix_vie);
	rect_choix_vie.y=350;
	SDL_SetRenderDrawColor(renderer, 0,0,0,0);
	SDL_RenderDrawRect(renderer, &rect_choix_vie);



	SDL_RenderPresent(renderer);
	
    



    
    while (running) 
    {
        while (SDL_PollEvent(&event))
        {
            switch(event.type)
            {
                

                case SDL_MOUSEBUTTONDOWN:
                    position_mouse.x=event.button.x;
                    position_mouse.y=event.button.y;

					if(SDL_PointInRect(&position_mouse,&rect_taille_5))
					{
						taille=5;
						rect_choix_taille.y=425;
						SDL_SetRenderDrawColor(renderer, 0,0,0,0);
						SDL_RenderDrawRect(renderer, &rect_choix_taille);
						rect_choix_taille.y=350;
						SDL_SetRenderDrawColor(renderer, 0,0,0,0);
						SDL_RenderDrawRect(renderer, &rect_choix_taille);

						rect_choix_taille.y=275;
						SDL_SetRenderDrawColor(renderer, 255,255,255,0);
						SDL_RenderDrawRect(renderer, &rect_choix_taille);
						
					}
					else if(SDL_PointInRect(&position_mouse,&rect_taille_10))
					{
						taille=10;
						rect_choix_taille.y=275;
						SDL_SetRenderDrawColor(renderer, 0,0,0,0);
						SDL_RenderDrawRect(renderer, &rect_choix_taille);
						rect_choix_taille.y=425;
						SDL_SetRenderDrawColor(renderer, 0,0,0,0);
						SDL_RenderDrawRect(renderer, &rect_choix_taille);

						rect_choix_taille.y=350;
						SDL_SetRenderDrawColor(renderer, 255,255,255,0);
						SDL_RenderDrawRect(renderer, &rect_choix_taille);
					}
					else if(SDL_PointInRect(&position_mouse,&rect_taille_15))
					{
						taille=15;
						rect_choix_taille.y=275;
						SDL_SetRenderDrawColor(renderer, 0,0,0,0);
						SDL_RenderDrawRect(renderer, &rect_choix_taille);
						rect_choix_taille.y=350;
						SDL_SetRenderDrawColor(renderer, 0,0,0,0);
						SDL_RenderDrawRect(renderer, &rect_choix_taille);

						rect_choix_taille.y=425;
						SDL_SetRenderDrawColor(renderer, 255,255,255,0);
						SDL_RenderDrawRect(renderer, &rect_choix_taille);
					}
					if(SDL_PointInRect(&position_mouse,&rect_avec_vie))
					{
						vie=1;
						rect_choix_vie.y=275;
						SDL_SetRenderDrawColor(renderer, 255,255,255,0);
						SDL_RenderDrawRect(renderer, &rect_choix_vie);
						rect_choix_vie.y=350;
						SDL_SetRenderDrawColor(renderer, 0,0,0,0);
						SDL_RenderDrawRect(renderer, &rect_choix_vie);
					}
					else if(SDL_PointInRect(&position_mouse,&rect_sans_vie))
					{
						vie=0;
						rect_choix_vie.y=350;
						SDL_SetRenderDrawColor(renderer, 255,255,255,0);
						SDL_RenderDrawRect(renderer, &rect_choix_vie);
						rect_choix_vie.y=275;
						SDL_SetRenderDrawColor(renderer, 0,0,0,0);
						SDL_RenderDrawRect(renderer, &rect_choix_vie);
					}
					SDL_RenderPresent(renderer);

                    if(SDL_PointInRect(&position_mouse,&rec_jouer))
                    {
						if(vie)
							sprintf(prog,"./PicrossErreur %d",taille);
						else
							sprintf(prog,"./PicrossUndo %d",taille);
                        system(prog);
                    }
					if(SDL_PointInRect(&position_mouse,&rect_info_mode))
					{
						SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,"Information","Avec : Fin du jeu au bout de 3 erreurs\n       ou le bon dessin est trouvé!\n\nSans : Fin du jeu si le bon dessin est trouvé!\n       (possibilité de revenir en arrière)\n\n                Bon courage :)",NULL);
					}
					if(SDL_PointInRect(&position_mouse,&rec_credit))
					{
						SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,"Crédit","Jeu réalisé dans le cadre d'un projet de première année d'école d'ingénieur en informatique [ISIMA]\n\nTuteur : Loic Yon\n\nEtudiantes : Abir Khémiri & Edith Bousquet ",NULL);
					}

                    break;







                case SDL_QUIT :
                    running = 0;
            }
        }
        SDL_Delay(1); //  delai minimal
		
    }
	SDL_DestroyTexture(avatar);
	TTF_CloseFont(font);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 1;
}