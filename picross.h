#ifndef picross_h
#define picross_h

#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>


#define POSITION 200

/*Initialisation tableau d'indice */
void init_tab_indice(int lar, int hau, int [hau][lar]);

/*Remplissage tableau d'indice (haut)*/
void remplir_tab_indice_haut(int grille_lar,int grille_supp,int grille_hau,int [grille_hau][grille_lar],int [grille_supp][grille_lar]);
/*Remplissage tableau d'indice (gauche)*/
void remplir_tab_indice_gauche(int grille_lar,int grille_supp,int grille_hau,int [grille_hau][grille_lar],int [grille_hau][grille_supp]);

/*Affichage de ma grille*/
void afficher_grille(int grille_lar,int grille_hau ,SDL_Rect [grille_hau][grille_lar],SDL_Rect ,SDL_Renderer * );
/*Affichage de la surgrille : bloc 5*5 (meilleur visibilité)*/
void afficher_grille_5(int ,int ,SDL_Renderer * );

/*Affichage de ma grille d'indice haut*/
void afficher_grille_indice_haut(int grille_supp,int grille_lar,int [grille_supp][grille_lar],SDL_Rect [grille_supp][grille_lar],SDL_Rect ,SDL_Renderer * );
/*Affichage de ma grille d'indice gauche*/
void afficher_grille_indice_gauche(int grille_hau,int grille_supp,int [grille_hau][grille_supp],SDL_Rect [grille_hau][grille_supp],SDL_Rect ,SDL_Renderer * );

/*DEssin d'une croix (pinceau)*/
void dessinCroix(SDL_Rect *, SDL_Renderer * );

/*Vérification : le bon dessin est trouvé ou non*/
int verif(int grille_hau,int grille_lar ,int [grille_hau][grille_lar],int [grille_hau][grille_lar]);


/*Affichage dans un rectangle*/
void affich_rect(char * text, SDL_Rect rect,SDL_Renderer * renderer,TTF_Font * font );
#endif
