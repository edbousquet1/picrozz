#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "picross.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

int main(int argc,char * argv[])
{
	/*choix fichie dessin grille*/
	srand(time(0));
	int num_fichier=(rand()%10)+1;
	char chemin[30];
	sprintf(chemin,"%d/%d.txt",atoi(argv[1]), num_fichier);

	/*Déclaration Background*/
    SDL_Texture *avatar;
	SDL_Rect rect;

    /*Déclaration de la grille*/
	int grille_lar = atoi(argv[1]);
	int grille_hau = atoi(argv[1]);
	SDL_Rect tabRec[grille_hau][grille_lar];
	int tab_dessin[grille_hau][grille_lar];

	 /*Déclaration du tableau verif*/
	int tab_verif[grille_hau][grille_lar];

	/*Position de la grille*/
	SDL_Rect rec;
    rec.x = POSITION;
    rec.y = POSITION;
    rec.w = 20;
    rec.h = 20;

	/*Déclaration des grilles avec les indices*/
	int grille_supp = (grille_lar/2) +1;
	int tab_indice_haut[grille_supp][grille_lar];
	int tab_indice_gauche[grille_hau][grille_supp];
	SDL_Rect tab_indice_rec_haut[grille_supp][grille_lar];
	SDL_Rect tab_indice_rec_gauche[grille_hau][grille_supp];
    
	/*Position de la grille avec les indices (haut)*/
	SDL_Rect rec_hau;

    rec_hau.x = POSITION ;
    rec_hau.y = POSITION - (20*grille_supp);
    rec_hau.w = 20;
    rec_hau.h = 20;

	/*Position de la grille avec les indices (gauche)*/
	SDL_Rect rec_gauche;
    rec_gauche.x = POSITION - (20*grille_supp);
    rec_gauche.y = POSITION;
    rec_gauche.w = 20;
    rec_gauche.h = 20;

	/*Position du pinceau remplissage*/

	/*0 = couleur ; 1 = croix ; 2 = blanc*/
	SDL_Rect rec_pinceau_couleur;
	rec_pinceau_couleur.x=POSITION + (20*grille_hau) +30;
	rec_pinceau_couleur.y=POSITION;
	rec_pinceau_couleur.w=20;
	rec_pinceau_couleur.h=20;

	SDL_Rect rec_pinceau_croix ;
	rec_pinceau_croix.x=POSITION + (20*grille_hau) +70;
	rec_pinceau_croix.y=POSITION;
	rec_pinceau_croix.w=20;
	rec_pinceau_croix.h=20;

	SDL_Rect rec_pinceau_blanc ;
	rec_pinceau_blanc.x=POSITION + (20*grille_hau) +110;
	rec_pinceau_blanc.y=POSITION;
	rec_pinceau_blanc.w=20;
	rec_pinceau_blanc.h=20;
	int couleurPinceau = 0;

	SDL_Rect indication_pinceau;
	indication_pinceau.x= POSITION + (20*grille_hau) +25;
	indication_pinceau.y= POSITION-5;
	indication_pinceau.w= 30;
	indication_pinceau.h= 30;

	/*Position des coeurs (vies)*/
	SDL_Rect rec_coeur1;
	rec_coeur1.x = POSITION;
    rec_coeur1.y = POSITION + (20*grille_hau)+25;
    rec_coeur1.w = 16;
    rec_coeur1.h = 16;

	SDL_Rect rec_coeur2;
	rec_coeur2.x = POSITION+20;
    rec_coeur2.y = POSITION + (20*grille_hau)+25;
    rec_coeur2.w = 16;
    rec_coeur2.h = 16;

	SDL_Rect rec_coeur3;
	rec_coeur3.x = POSITION+40;
    rec_coeur3.y = POSITION + (20*grille_hau)+25;
    rec_coeur3.w = 16;
    rec_coeur3.h = 16;

	/*Déclaration des éléments pour afficher du texte*/
	SDL_Surface *image;
	SDL_Texture *icon;
	
	/*Déclaration des indices de boucle*/
    int i, j, k;

	/*Déclaration du booléen si une case a était coché*/
	int cleacked = 0;

	/*Déclaration du compteur d'erreur*/
	SDL_Rect rec_init;
	rec_init.x = 0;
    rec_init.y = 0;
    rec_init.w = 0;
    rec_init.h = 0;
	SDL_Rect tab_erreur[3] = {rec_init, rec_init, rec_init};
	int erreurExiste = 0;
	int erreur = 0;

	
	
	/*Déclaration de la fenetre*/
    SDL_Window *window;

	/*Déclaration du pinceau*/
	SDL_Renderer *renderer;
    
	/*Déclaration de la position de la souris*/
	SDL_Point position_mouse;
    
	/*Déclaration des éléments de la fenetre*/
   	int running = 1, width = 700, height = 700;

	/*Déclaration du fichier contenant le dessin*/
	FILE *file = fopen(chemin,"r");

	/*Déclaration des événements*/
	SDL_Event event;
	
	
	
/*******************************************************************************/
/*******************************************************************************/

	if (file)
	{
		i = 0;
		
		while(!feof(file))
		{
			/*Boucle permettant de remplir le tableau avec le dessin*/
			for(j=0; j<grille_lar; j++)
			{
				
				fscanf(file, "%d ", &tab_dessin[j][i]);
			}
            i++;
		}
		fclose(file);

		/*Initialisation tableau d'indice (haut)*/
		
		init_tab_indice(grille_lar,grille_supp,tab_indice_haut);


		/*Initialisation tableau d'indice (gauche)*/
		
		init_tab_indice(grille_supp,grille_hau,tab_indice_gauche);

		
		/*Remplissage tableau d'indice (haut)*/
		remplir_tab_indice_haut(grille_lar,grille_supp,grille_hau,tab_dessin,tab_indice_haut);


		
		
		/*Remplissage tableau d'indice (gauche)*/
		remplir_tab_indice_gauche(grille_lar,grille_supp,grille_hau,tab_dessin,tab_indice_gauche);

		/*Initialisation tableau de vérification*/
		
		init_tab_indice(grille_lar,grille_hau,tab_verif);
	
	} 
	
	/*Initialisation du SDL*/
	if (SDL_Init(SDL_INIT_EVERYTHING) == -1)
	{
		fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError());
		return EXIT_FAILURE;
	}

	/*Initialisation du TTF*/
	if (TTF_Init() != 0)
	{
		fprintf(stderr, "Erreur d'initialisation TTF : %s\n", TTF_GetError());
	}


	/*Initialisation de la fenetre*/
	window = SDL_CreateWindow("PicroZZ", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_RESIZABLE);
	if (window == 0)
	{
		fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError());
	}
	
	/*Initialisation du pinceau*/
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED ); /*  SDL_RENDERER_SOFTWARE */
    if (renderer == 0) {
		 fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError());
		 /* faire ce qu'il faut pour quitter proprement */
	}
	SDL_SetWindowResizable(window,0);
	/*Affichage Background*/
	image=IMG_Load("image/fondJeu.jpg");
	
    avatar = SDL_CreateTextureFromSurface(renderer, image);
	SDL_FreeSurface(image);
	rect.x = 0;
	rect.y = 0;
	rect.w = 700;
    rect.h = 700;
	SDL_RenderCopy(renderer, avatar, NULL, &rect);
	SDL_RenderPresent(renderer);
	/*Initialisation de la couleur du fond
    SDL_SetRenderDrawColor(renderer,255,255,255,0);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);*/


	/*Affichage de ma grille*/
	afficher_grille(grille_lar,grille_hau,tabRec,rec,renderer);
	afficher_grille_5(grille_lar,grille_hau,renderer);

	/*Couleur des grilles des indices*/
	 SDL_SetRenderDrawColor(renderer,167,155,152,0);
	/*Affichage de ma grille d'indice haut*/
	afficher_grille_indice_haut(grille_supp,grille_lar,tab_indice_haut,tab_indice_rec_haut,rec_hau,renderer);
	
	/*Affichage de ma grille d'indice gauche*/
	afficher_grille_indice_gauche(grille_hau,grille_supp,tab_indice_gauche,tab_indice_rec_gauche,rec_gauche,renderer);
	SDL_RenderPresent(renderer);
		/*Affichage option choix pinceau*/
	/*Pinceau Croix*/
	dessinCroix(&(rec_pinceau_croix), renderer);
	/*Pinceau couleur*/
	SDL_SetRenderDrawColor(renderer, 0,0,0,0);
	SDL_RenderFillRect(renderer, &(rec_pinceau_couleur));
	/*Pinceau blanc*/
	SDL_SetRenderDrawColor(renderer, 255,255,255,0);
	SDL_RenderFillRect(renderer, &(rec_pinceau_blanc));
	
	/*Indication Pinceau */
	SDL_SetRenderDrawColor(renderer, 147,38,214,0);
	SDL_RenderDrawRect(renderer, &(indication_pinceau));

	indication_pinceau.x= POSITION + (20*grille_hau) +65;
	SDL_SetRenderDrawColor(renderer, 255,255,255,0);
	SDL_RenderDrawRect(renderer, &(indication_pinceau));

	indication_pinceau.x= POSITION + (20*grille_hau) +105;
	SDL_SetRenderDrawColor(renderer, 255,255,255,0);
	SDL_RenderDrawRect(renderer, &(indication_pinceau));
	
	SDL_RenderPresent(renderer);

	

	/*Affichage vies*/
	image = IMG_Load("image/like.png");
	icon = SDL_CreateTextureFromSurface(renderer, image);
	SDL_FreeSurface(image);
	SDL_RenderCopy(renderer, icon, NULL, &(rec_coeur1));
	SDL_RenderCopy(renderer, icon, NULL, &(rec_coeur2));
	SDL_RenderCopy(renderer, icon, NULL, &(rec_coeur3));
	SDL_RenderPresent(renderer);

	
 /*******************************************************************************/
/*******************************************************************************/

    /*Boucle des événements*/
	while (running) {
		while (SDL_PollEvent(&event) && erreur<3)
		{
			switch(event.type)
			{
				
/*******************************************************************************/
/*******************************************************************************/
				
				case SDL_MOUSEBUTTONDOWN:
					position_mouse.x = event.button.x;
					position_mouse.y = event.button.y;
					i = 0;
					

					/*Action : changement de pinceau*/
					if(SDL_PointInRect(&position_mouse, &rec_pinceau_couleur))
					{
						couleurPinceau = 0;
						indication_pinceau.x= POSITION + (20*grille_hau) +65;
						SDL_SetRenderDrawColor(renderer, 255,255,255,0);
						SDL_RenderDrawRect(renderer, &(indication_pinceau));

						indication_pinceau.x= POSITION + (20*grille_hau) +105;
						SDL_SetRenderDrawColor(renderer, 255,255,255,0);
						SDL_RenderDrawRect(renderer, &(indication_pinceau));

						indication_pinceau.x= POSITION + (20*grille_hau) +25;
						SDL_SetRenderDrawColor(renderer, 147,38,214,0);
						SDL_RenderDrawRect(renderer, &(indication_pinceau));
					
					
						SDL_RenderPresent(renderer);

					}
					else if(SDL_PointInRect(&position_mouse, &rec_pinceau_croix))
					{
						couleurPinceau = 1;
						indication_pinceau.x= POSITION + (20*grille_hau) +25;
						SDL_SetRenderDrawColor(renderer, 255,255,255,0);
						SDL_RenderDrawRect(renderer, &(indication_pinceau));

						indication_pinceau.x= POSITION + (20*grille_hau) +105;
						SDL_SetRenderDrawColor(renderer, 255,255,255,0);
						SDL_RenderDrawRect(renderer, &(indication_pinceau));

						indication_pinceau.x= POSITION + (20*grille_hau) +65;
						SDL_SetRenderDrawColor(renderer, 147,38,214,0);
						SDL_RenderDrawRect(renderer, &(indication_pinceau));
						SDL_RenderPresent(renderer);
					}
					else if(SDL_PointInRect(&position_mouse, &rec_pinceau_blanc))
					{
						couleurPinceau = 2;

						indication_pinceau.x= POSITION + (20*grille_hau) +65;
						SDL_SetRenderDrawColor(renderer, 255,255,255,0);
						SDL_RenderDrawRect(renderer, &(indication_pinceau));

						indication_pinceau.x= POSITION + (20*grille_hau) +25;
						SDL_SetRenderDrawColor(renderer, 255,255,255,0);
						SDL_RenderDrawRect(renderer, &(indication_pinceau));
						indication_pinceau.x= POSITION + (20*grille_hau) +105;
						SDL_SetRenderDrawColor(renderer, 147,38,214,0);
						SDL_RenderDrawRect(renderer, &(indication_pinceau));
						SDL_RenderPresent(renderer);
					}

					
					else
					{
						
						/*Boucle de recherche de la case sélectionnée*/
						while(i<grille_lar && !cleacked)
						{
							j = 0;
							while(j<grille_hau && !cleacked)
							{
								cleacked=SDL_PointInRect(&position_mouse, &(tabRec[i][j]));
								j++;
							}
							i++;
							
						}
						if(cleacked)
						{

							if(!couleurPinceau)
							{
								/*Coloration de la case selectionnée*/
								if(tab_dessin[i-1][j-1])
								{
									SDL_SetRenderDrawColor(renderer, 0,0,0,0);
									tab_verif[i-1][j-1]=1;
								}
								else
								{
									k=0;
									while(k<3 && !erreurExiste)
									{
										if((tabRec[i-1][j-1].x == tab_erreur[k].x) && (tabRec[i-1][j-1].y == tab_erreur[k].y))
										{
											erreurExiste = 1;
										}
										k++;
									}
									if(!erreurExiste && erreur<3)
									{
										tab_erreur[erreur] = tabRec[i-1][j-1];
										erreur++;
									}
									SDL_SetRenderDrawColor(renderer, 255, 0, 0, 0);
									erreurExiste=0;
									
								}
								SDL_RenderFillRect(renderer, &(tabRec[i-1][j-1]));
								SDL_SetRenderDrawColor(renderer, 255,255,255,0);
								SDL_RenderDrawRect(renderer, &(tabRec[i-1][j-1]));
								SDL_RenderPresent(renderer);
								if(verif(grille_hau,grille_lar,tab_dessin,tab_verif) && erreur<3)
								{
									SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,"BRAVO !","Vous avez gagné !!!",NULL);
									running=0;
								}
							}
							else if(couleurPinceau==1)
							{
								k=0;
								while(k<3 && !erreurExiste)
								{
									if((tabRec[i-1][j-1].x == tab_erreur[k].x) && (tabRec[i-1][j-1].y == tab_erreur[k].y))
									{
										erreurExiste = 1;
									}
									k++;
								}
								if(!erreurExiste)
								{
									SDL_SetRenderDrawColor(renderer, 255,255,255,0);
									SDL_RenderFillRect(renderer, &(tabRec[i-1][j-1]));
									dessinCroix(&(tabRec[i-1][j-1]), renderer);
									tab_verif[i-1][j-1]=0;
								}
								erreurExiste = 0;


							}
							else
							{
								k=0;
								while(k<3 && !erreurExiste)
								{
									if((tabRec[i-1][j-1].x == tab_erreur[k].x) && (tabRec[i-1][j-1].y == tab_erreur[k].y))
									{
										erreurExiste = 1;
									}
									k++;
								}
								if(!erreurExiste)
								{
									SDL_SetRenderDrawColor(renderer, 255,255,255,0);
									SDL_RenderFillRect(renderer, &(tabRec[i-1][j-1]));
									SDL_SetRenderDrawColor(renderer, 0,0,0,0);
									SDL_RenderDrawRect(renderer, &(tabRec[i-1][j-1]));
									tab_verif[i-1][j-1]=0;
								}
								erreurExiste = 0;
								SDL_RenderPresent(renderer);
							}

								
						}
						
						
					}
					cleacked=0;
					switch(erreur)
					{
						case 1 :
							SDL_SetRenderDrawColor(renderer, 255,255,255,0);
							SDL_RenderFillRect(renderer, &(rec_coeur1));
							SDL_RenderPresent(renderer);

							image = IMG_Load("image/broken-heart.png");
							icon = SDL_CreateTextureFromSurface(renderer, image);
							SDL_FreeSurface(image);
							SDL_RenderCopy(renderer, icon, NULL, &(rec_coeur1));
							SDL_RenderPresent(renderer);
							break;
						case 2 :
							SDL_SetRenderDrawColor(renderer, 255,255,255,0);
							SDL_RenderFillRect(renderer, &(rec_coeur2));
							SDL_RenderPresent(renderer);

							image = IMG_Load("image/broken-heart.png");
							icon = SDL_CreateTextureFromSurface(renderer, image);
							SDL_FreeSurface(image);
							SDL_RenderCopy(renderer, icon, NULL, &(rec_coeur2));
							SDL_RenderPresent(renderer);
							break;
						case 3 :
							SDL_SetRenderDrawColor(renderer, 255,255,255,0);
							SDL_RenderFillRect(renderer, &(rec_coeur3));
							SDL_RenderPresent(renderer);

							image = IMG_Load("image/broken-heart.png");
							icon = SDL_CreateTextureFromSurface(renderer, image);
							SDL_FreeSurface(image);
							SDL_RenderCopy(renderer, icon, NULL, &(rec_coeur3));
							SDL_RenderPresent(renderer);
							SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,"ATTENTION !","Vous avez perdu...",NULL);
							
							running=0;
							break;
					}
					break;

/*******************************************************************************/
/*******************************************************************************/

			case SDL_QUIT :
				running = 0;
			}
		}
		SDL_Delay(1); // delai minimal
	}
   

	
	SDL_FreeSurface(image);

	SDL_DestroyTexture(avatar);
	SDL_DestroyTexture(icon);

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);

    SDL_Quit();
 
    return EXIT_SUCCESS;
}