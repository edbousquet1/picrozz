
#include "picross.h"
/*Initialisation tableau d'indice */
void init_tab_indice(int lar, int hau, int  tab_indice[hau][lar])
{
    int i,j;
    for(i=0; i<hau; i++)
		{
			for(j=0; j<lar; j++)
			{
				tab_indice[i][j] = 0;
			}
		}
}

/*Remplissage tableau d'indice (haut)*/
void remplir_tab_indice_haut(int grille_lar,int grille_supp,int grille_hau,int tab_dessin[grille_hau][grille_lar],int tab_indice_haut[grille_supp][grille_lar])
{
    int compteur;
	int indice;
    int i,j;
    for(j=0; j<grille_lar; j++)
    {	
        indice = grille_supp -1;
        i = grille_hau -1;
        while(i>=0)
        {
            compteur = 0;
            while((i>=0) && (tab_dessin[j][i] == 0))
            {
                i--;
            }
            if(tab_dessin[j][i] == 1)
            {
                while((i>=0) && (tab_dessin[j][i]== 1))
                {
                    compteur++;
                    i--;
                }
                tab_indice_haut[indice][j] = compteur;
                indice--;

            }
        }
    }
}


/*Remplissage tableau d'indice (gauche)*/
void remplir_tab_indice_gauche(int grille_lar,int grille_supp,int grille_hau,int tab_dessin[grille_hau][grille_lar],int tab_indice_gauche[grille_hau][grille_supp])
{
    int compteur;
	int indice;
    int i,j;
    for(j=0; j<grille_hau; j++)
    {	
        indice = grille_supp-1;
        i = grille_lar -1;
        while(i>=0)
        {
            compteur = 0;
            while((i>=0) && (tab_dessin[i][j] == 0))
            {
                i--;
            }
            if(tab_dessin[i][j] == 1)
            {
                while((i>=0) && (tab_dessin[i][j] == 1))
                {
                    compteur++;
                    i--;
                }
                tab_indice_gauche[j][indice] = compteur;
                indice--;

            }
        }
    }
}
/*Affichage de ma grille*/
void afficher_grille(int grille_lar,int grille_hau,SDL_Rect tabRec[grille_hau][grille_lar],SDL_Rect rec,SDL_Renderer * renderer)
{
    int i,j;
    /*Couleur de la grille*/
    SDL_SetRenderDrawColor(renderer,167,155,152,0);
  
	/*Boucle d'affichage de ma grille*/
    for (i=0; i<grille_lar; i++)
    {
        for (j=0; j<grille_hau; j++)
        {		
            tabRec[i][j]=rec;
            SDL_RenderDrawRect(renderer,&rec);
            rec.y+=20;
		}
        rec.x+=20;
        rec.y=POSITION;
    }
    SDL_RenderPresent(renderer);
}

/*Affichage de la surgrille : bloc 5*5 (meilleur visibilité)*/
void afficher_grille_5(int grille_lar,int grille_hau,SDL_Renderer * renderer)
{
    int i,j;
    SDL_Rect rec;
    rec.x = POSITION;
    rec.y = POSITION;
    rec.w = 20*5;
    rec.h = 20*5;
    /*Couleur de la grille*/
    SDL_SetRenderDrawColor(renderer,0, 0, 0, 0);
  
	/*Boucle d'affichage de ma grille*/
    for (i=0; i<grille_lar; i+=5)
    {
        for (j=0; j<grille_hau; j+=5)
        {	
            SDL_RenderDrawRect(renderer,&rec);
            rec.y+=20*5;
		}
        rec.x+=20*5;
        rec.y=POSITION;
    }
    SDL_RenderPresent(renderer);
}

/*Affichage de ma grille d'indice haut*/
void afficher_grille_indice_haut(int grille_supp,int grille_lar,int tab_indice_haut[grille_supp][grille_lar],SDL_Rect tab_indice_rec_haut[grille_supp][grille_lar],SDL_Rect rec_hau,SDL_Renderer * renderer)
{
    int i,j;
    /*Déclaration des éléments pour afficher du texte*/
	SDL_Surface *surfaceMessage;
	SDL_Texture *message;
	char text[3];
    /*Déclaration des couleurs*/
	SDL_Color Noir = {0, 0, 0, 0};

    /*Récupération de la police*/
	TTF_Font* font = TTF_OpenFont("font/Arial.ttf", 40);
    
    /*Couleur des grilles des indices*/
	 SDL_SetRenderDrawColor(renderer,167,155,152,0);
  
	/*boucle affichage grille haut*/
    for (i=0; i<grille_supp; i++)
    {
        for (j=0; j<grille_lar; j++)
        {		
            tab_indice_rec_haut[i][j] = rec_hau;
            SDL_RenderDrawRect(renderer, &rec_hau);
			if(tab_indice_haut[i][j] != 0)
			{
				sprintf(text, "%d", tab_indice_haut[i][j]);
				surfaceMessage = TTF_RenderText_Solid(font, text, Noir);
				message = SDL_CreateTextureFromSurface(renderer, surfaceMessage);
				SDL_RenderCopy(renderer, message, NULL, &rec_hau);
			}
            rec_hau.x += 20;
        }
        rec_hau.y += 20;

        rec_hau.x = POSITION;
    }
    /*Libération*/
    TTF_CloseFont(font);
    SDL_FreeSurface(surfaceMessage);
    SDL_DestroyTexture(message);
    
}
/*Affichage de ma grille d'indice gauche*/
void afficher_grille_indice_gauche(int grille_hau,int grille_supp,int tab_indice_gauche[grille_hau][grille_supp],SDL_Rect tab_indice_rec_gauche[grille_hau][grille_supp],SDL_Rect rec_gauche,SDL_Renderer * renderer)
{
    int i,j;
    /*Déclaration des éléments pour afficher du texte*/
	SDL_Surface *surfaceMessage;
	SDL_Texture *message;
	char text[3];
    /*Déclaration des couleurs*/
	SDL_Color Noir = {0, 0, 0, 0};

    /*Récupération de la police*/
	TTF_Font* font = TTF_OpenFont("font/Arial.ttf", 40);
    
    /*Couleur des grilles des indices*/
	 SDL_SetRenderDrawColor(renderer,167,155,152,0);
     
  /*boucle affichage grille gauche*/
    for (i=0; i<grille_hau; i++)
    {

        for (j=0; j<grille_supp; j++)
        {		
            tab_indice_rec_gauche[i][j] = rec_gauche;
            SDL_RenderDrawRect(renderer, &rec_gauche);
			if(tab_indice_gauche[i][j] != 0)
			{
				sprintf(text, "%d", tab_indice_gauche[i][j]);
				surfaceMessage = TTF_RenderText_Solid(font, text, Noir);
				message = SDL_CreateTextureFromSurface(renderer, surfaceMessage);
				SDL_RenderCopy(renderer, message, NULL, &rec_gauche);
			}
            rec_gauche.x += 20;
        }
        rec_gauche.y += 20;
        rec_gauche.x = POSITION - (20*grille_supp);
    }
    /*Libération*/
    TTF_CloseFont(font);
    SDL_DestroyTexture(message);
    SDL_FreeSurface(surfaceMessage);
    
}

/*DEssin d'une croix (pinceau)*/
void dessinCroix(SDL_Rect* rec, SDL_Renderer * renderer)
{
   SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
    SDL_RenderDrawLine(renderer, rec->x,rec->y,rec->x+rec->w,rec->y+rec->h);
	SDL_RenderDrawLine(renderer, rec->x+rec->w,rec->y,rec->x,rec->y+rec->h);
	SDL_SetRenderDrawColor(renderer, 0,0,0,0);
	SDL_RenderDrawRect(renderer, rec);

	SDL_RenderPresent(renderer);
}

/*Vérification : le bon dessin est trouvé ou non*/
int verif(int grille_hau,int grille_lar,int tab_dessin[grille_hau][grille_lar],int tab_verif[grille_hau][grille_lar])
{
    int i=0,
        j=0,
        rep=1;

    while(i<grille_hau && rep)
    {
        while(j<grille_lar && rep)
        {
            if(tab_dessin[i][j] != tab_verif[i][j])
            {
                rep=0;
            }
            j++;
        }
        j=0;
        i++;
    }
    return rep;
}

/*Affichage dans un rectangle*/
void affich_rect(char * text, SDL_Rect rect,SDL_Renderer * renderer,TTF_Font * font )
{
    /*Déclaration des couleurs*/
	SDL_Color Blanc = {255, 255, 255, 0};
    /*Déclaration des éléments pour afficher du texte*/
	SDL_Surface *surfaceMessage;
	SDL_Texture *message;

    
	surfaceMessage = TTF_RenderText_Solid(font, text, Blanc);
    message = SDL_CreateTextureFromSurface(renderer, surfaceMessage);
	SDL_RenderCopy(renderer, message, NULL, &rect);
	SDL_RenderPresent(renderer);

    SDL_DestroyTexture(message);
    SDL_FreeSurface(surfaceMessage);

}